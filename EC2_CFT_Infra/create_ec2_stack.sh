#!/bin/bash

# Submits aws command to create the 3 EC2 instances required for the capstone project
# Required input parameter of the stack name

stack_name=$1

if [ -z "$1" ]; then
	echo "Usage:  ./create_ec2_stack.sh <STACKNAME>"
	exit 1
else
	aws cloudformation create-stack --template-body file://EC2_Builds.yaml --stack-name ${stack_name} --parameters ParameterKey=KeyName,ParameterValue=mark-beth-key
	exit 0
fi
