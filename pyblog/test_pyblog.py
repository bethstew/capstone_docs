import unittest
import pyblog


class Testpyblog(unittest.TestCase):

    def test_get_args(self):
        action, user_file = pyblog.get_args('unittest')
        self.assertEqual(action, None)

    def test_get_creds(self):
        user, passwd, url = pyblog.get_creds('unittest')
        self.assertEqual(user, None)

    def test_invalid_creds(self):
        user = 'wordpress'
        passwd = 'wrongpassword'
        url = 'http://18.191.204.40:8088/wp-json/wp/v2/posts'
        result = pyblog.get_last_blog(user, passwd, url)
        assert "Unauthorized to access Wordpress" in result

    def test_wp_unavailable_read(self):
        user, passwd, url = pyblog.get_creds('unittest')
        result = pyblog.get_last_blog(user, passwd, url)
        assert "Wordpress is not available" in result

    def test_wp_unavailable_post(self):
        user, passwd, url = pyblog.get_creds('unittest')
        user_file = 'test_pyblog.txt'
        result = pyblog.upload_blog_post(user, passwd, url, user_file)
        assert "Wordpress is not available" in result

    def test_get_last_blog(self):
        user = 'wordpress'
        passwd = 'wordpress'
        url = 'http://18.191.204.40:8088/wp-json/wp/v2/posts'
        result = pyblog.get_last_blog(user, passwd, url)
        assert "link" in result

    def test_print_last_blog_unauthorized_Exception(self):
        user = 'wordpress'
        passwd = 'invalidpasswd'
        url = 'http://18.191.204.40:8088/wp-json/wp/v2/posts'
        un_exp = (pyblog.print_last_blog(pyblog.get_last_blog(user, passwd, url)))
        print(un_exp)

    def test_print_last_blog_WP_not_avail_Exception(self):
        user = 'wordpress'
        passwd = 'invalidpasswd'
        url = 'badurl'
        not_avail_exp = (pyblog.print_last_blog(pyblog.get_last_blog(user, passwd, url)))
        print(not_avail_exp)


if __name__ == "__main__":
    unittest.main()
